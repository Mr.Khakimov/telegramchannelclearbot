# telegramChannelClearBot

# Telegram Bot Project

This project is a Telegram bot written in Python using the Telebot library. It performs various functions such as providing daily forecasts, handling new chat members, and responding to user commands.

## Installation

1. Clone the repository to your local machine:

```
git clone https://gitlab.com/Mr.Khakimov/telegramchannelclearbot.git
```

2. Navigate to the project directory:

```
cd telegramchannelclearbot
```

3. Install the required Python packages using pip:

```
pip install -r requirements.txt
```

## Configuration

1. Obtain a Telegram API key from the BotFather on Telegram.
2. Create a `config.py` file in the project directory and add your API key:

```python
API_KEY = "your_telegram_api_key"
```

## Usage
Run the bot using the following command:

```python
python3 main.py
```

## File Structure
main.py: Main script containing bot functionality.

config.py: Configuration file with API key.

messages.py: File containing message templates.

utils.py: Utility functions and constants.

requirements.txt: File listing all required Python packages.
