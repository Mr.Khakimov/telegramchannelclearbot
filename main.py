import logging
import telebot
from config import API_KEY, valid_users
from messages import messages
from utils import rules

bot = telebot.TeleBot(API_KEY)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

def delete_message(chat_id, message_id):
    try:
        bot.delete_message(chat_id, message_id)
    except telebot.apihelper.ApiTelegramException as e:
        print(f"Error deleting message: {e}")

try:
    @bot.message_handler(commands=['start'])
    def start(message):
        bot.send_message(message.chat.id, messages['start'])


    @bot.message_handler(commands=['help'])
    def help(message):
        bot.send_message(message.chat.id, "Bu bot telegram kanal va gruhlardagi keraksiz narsalarni ochirish uchun ishlatiladi")


    @bot.message_handler(content_types=rules)
    def handle_new_chat_member(message):
        try:
            bot.delete_message(message.chat.id, message.message_id)
            return 0
        except telebot.apihelper.ApiTelegramException as e:
            print(f"Error deleting message: {e}")
        pass

    @bot.message_handler(commands=['prognoz'])
    def prognoz(message):
        import random

        phrases = [
            "Сегодня твой день будет удачным!",
            "Ожидай непредвиденных сюрпризов.",
            "Сегодня тебя ждет встреча с интересным человеком.",
            "Будь осторожен на дороге, возможны неприятности.",
            "Задумайся над своими целями и планами.",
            "Сегодня - отличный день для новых начинаний.",
            "Подари себе минутку покоя и расслабления.",
            "Не забудь позвонить близким и сказать им о своей любви.",
            "Сегодня ты сможешь решить сложную задачу.",
            "Будь проще в общении с окружающими.",
            "Скоро наступит изменение в лучшую сторону.",
            "Подумай о своем здоровье и питании.",
            "Сегодня откроются новые горизонты для творчества.",
            "Уделите внимание своим близким.",
            "Не стоит переживать по пустякам.",
            "Сегодня удача будет на твоей стороне.",
            "Будь готов к неожиданным приключениям.",
            "Возможно, тебя ждет приятное событие.",
            "Сегодня - день для саморазвития.",
            "Никогда не поздно начать что-то новое."
        ]

        random_phrase = random.choice(phrases)

        bot.send_message(message.chat.id, f"Прогноз на день: {random_phrase}")
        pass

    @bot.message_handler(func=lambda message: True)
    def echo_all(message):
        try:
            chat_id = message["chat"]["id"]
            bot.send_message(chat_id, f"{message.text} -  bu matnni tushunmadim, \n /help - Yordam")
            bot.send_message(valid_users[0], f" Chat or Chat_ID not Found!: \n{message.text}\n")
        except Exception as err:
            bot.send_message(valid_users[0], f" Chat or Chat_ID not Found!: \n{err}\n")
            return {"ok": False}
        try:
            user_id = message["from"]["id"]
        except Exception as err:
            bot.send_message(valid_users[0], f" Chat or Chat_ID not Found!: \n{err}\n")
            return {"ok": False}

    bot.polling()

except telebot.apihelper.ApiTelegramException as e:
    print(f"Telegram API Error: {e}")
    for user_id in valid_users:
        bot.send_message(user_id, f"Произошла ошибка в работе бота: {e}")

if __name__ == '__main__':
    try:
        bot.polling()
        for user_id in valid_users:
            bot.send_message(user_id, f"Произошла ошибка в работе бота: {e}")
    except Exception as e:
        for user_id in valid_users:
            bot.send_message(user_id, f"Произошла ошибка в работе бота: {e}")